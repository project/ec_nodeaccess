Ecommerce Node Access Product Module

danielb@aspedia.net 
http://www.aspedia.net

Define nodes which have 'purchasable access'; these nodes cannot be seen by anyone except for the Author, Administrators, and anyone who purchases the relevant 'Node Access' product.  'Node Access' products grant view access to 'purchasable access' nodes, either by category or by node.  If the 'Node Access' product is also an 'EC Recurring' product, then view access will be granted for nodes published during the period of the recurring purchase.
